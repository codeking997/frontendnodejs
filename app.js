const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;


let os = require("os");

//starts the server
  const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello World');
  });



//listens for the server
server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
'use strict';

const fs = require('fs');
//prints the contents of the student json file. 
let rawdata = fs.readFileSync('student.json');
let student = JSON.parse(rawdata);
console.log(student);

const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
});
/*
this method asks the user to provide some options in the terminal
if input is one it will print the contents of the student.json file
if 2 it will print information on the platform specifics
and if 3 it will either start the server or tell the user that server has 
already started
*/
function getInput(){
  console.log("Choose an option:\n" +
      "1. Read package.json\n" +
      "2. Dispay OS info\n" +
      "3. Start http server");    readline.question("What you want to do?",  input => {
      if(input === "1"){
          const file = fs.readFileSync('student.json', 'utf8');
          console.log(file);
      }else if(input === "2"){
        console.log("Platform: " + os.platform());
        console.log("Architecture: " + os.arch());
        console.log("system memory: " + os.totalmem());
        console.log("free memory: " + os.freemem());
        console.log("release: " + os.release());
        console.log("CPU cores: " + os.endianness());
        console.log("user: " + os.userInfo());
        console.log("type: " + os.type());
      }else if(input === "3"){
        if(server.listening){
          console.log("Server has already been started");
        }else {
          server.listen(port, hostname, () =>{
            console.log(`Server running at http://${hostname}:${port}/`);
          });
        }
        
      }
      getInput();
  });
}
getInput();

